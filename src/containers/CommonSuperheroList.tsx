import * as React from 'react';
import { connect } from 'react-redux';
import SuperheroList from '../components/SuperheroList';

import { IError } from '../common/interfaces/IError';
import SuperheroDetail from '../components/SuperheroDetail';
import { getSuperheroes, selectSuperhero } from '../store/actions/superheroes';
import { selectData, selectError, selectHighlighted, selectLoading } from '../store/reducers/superheroes-selectors';

import './CommonSuperheroList.css';
import Header from "../components/Header";
import {searchSuperheroes} from "../services/superhero.service";

interface ICommonSuperheroListProps {
    data: [];
    loading: boolean;
    error: IError;
    onButtonClick: (query: string) => void;
    onSelectedHero: (id: number) => void;
    selectedHero?: number;
}

export class CommonSuperheroList extends React.Component<ICommonSuperheroListProps> {
    public render() {
        const { data, loading, error, onButtonClick, onSelectedHero, selectedHero } = this.props;
        console.log('aloha', data);
        const dataObject: any = data;

        if (loading) {
            return <p>LOADING...</p>;
        }

        return(
            <div className='Container'>
                <Header onButtonClick={(name: string) => onButtonClick(name)} />
                <div className='ListContainer'>
                    <SuperheroList superheroes={data} onSelectHero={(id: any) => onSelectedHero(id)}/>
                </div>
                { selectedHero &&
                    <div className='DetailContainer'>
                        <SuperheroDetail detail={dataObject ? dataObject.filter((hero: any) => hero.id === selectedHero)[0] : {}}/>
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    data: selectData(state),
    error: selectError(state),
    loading: selectLoading(state),
    selectedHero: selectHighlighted(state),
});

const mapDispatchToProps = (dispatch: any) => ({
    onButtonClick(query: string) {
        console.log('lalal', query);
        dispatch(getSuperheroes(query));
    },
    onSelectedHero(id: number) {
        dispatch(selectSuperhero(id));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CommonSuperheroList);
