import React from 'react';
import ReactDOM from 'react-dom';

import { ConnectedRouter } from 'connected-react-router';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

import App from './components/App';
import './index.css';
import * as serviceWorker from './serviceWorker';
import rootReducer from './store/reducers/index';

import { initStore } from './store';

const history = createHistory();
const store = initStore(rootReducer, history);
declare const module: any;

const render = () => {
    ReactDOM.render(
        (
            <Provider store={store}>
                    <App />
            </Provider>
        ),
        document.getElementById('root'),
    );
};

render();

if (module.hot) {
    module.hot.accept('./components/App', () => {
        console.log('updating App');
        render();
    });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
