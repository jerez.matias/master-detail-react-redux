import React from 'react';
let value: string;
const Header = (props: any) => (
    <div>
        <label>
            Buscar:
            <input type='text' onChange={(e) => value = e.target.value} />
        </label>
        <button onClick={() => props.onButtonClick(value)}>
            Click me to get data
        </button>
    </div>
);

export default Header;
