import React from 'react';
import CommonSuperheroList from '../containers/CommonSuperheroList';
import logo from '../logo.svg';
import './App.css';

const App: React.FC = () => {
  return (
    <div className='App'>
      <section>
        <CommonSuperheroList />
      </section>
    </div>
  );
};

export default App;
