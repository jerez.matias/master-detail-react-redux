import PropTypes from 'prop-types';
import React from 'react';
import Superhero from './Superhero';

const SuperheroList = (props: any) => (
    <ul>
        {
            props.superheroes.map((hero: any) =>
                <Superhero
                    key={hero.id}
                    hero={hero}
                    onClick={() => props.onSelectHero(hero.id)}
                />,
            )
        }
    </ul>
);

export default SuperheroList;
