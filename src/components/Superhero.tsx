import PropTypes from 'prop-types';
import React from 'react';
import './Superhero.css';

const Superhero = (props: any) => (
    <li className='Superhero' onClick={props.onClick} >
        <h4>{props.hero.name}</h4>
        <p><label>Full Name:&nbsp;</label><span>{props.hero.biography['full-name']}</span></p>
        <img className='ImageContainer' src={props.hero.image.url} />
    </li>
);

export default Superhero;
