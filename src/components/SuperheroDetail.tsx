import PropTypes from 'prop-types';
import React from 'react';
import Superhero from './Superhero';

const SuperheroDetail = (props: any) => (
    <div>
        <h3>{props.detail.name}</h3>
        <img src={props.detail.image.url}/>
    </div>
);

export default SuperheroDetail;
