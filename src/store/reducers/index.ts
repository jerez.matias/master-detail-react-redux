import { combineReducers } from 'redux-immutable';

import { routerReducer as router } from 'react-router-redux';
import superheroesReducer from './superheroes';

export default combineReducers({
    router,
    superheroesReducer,
});
