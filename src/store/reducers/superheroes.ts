import { fromJS } from 'immutable';

import { IAction } from '../../common/interfaces/IAction';
import { FETCHING_SUPERHEROES, FETCHING_SUPERHEROES_ERROR, FETCHING_SUPERHEROES_SUCCESS,
    SELECT_SUPERHERO } from '../actions/superheroes';

const initialState = fromJS({
    data: [],
    loading: false,
    error: false,
    selectedHero: null,
});

const superheroesReducer = (state = initialState, action: IAction<any>) => {
    switch (action.type) {
        case FETCHING_SUPERHEROES:
            return {
                ...state,
                loading: true,
                error: false,
                selectedHero: false,
            };
        case FETCHING_SUPERHEROES_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.payload.data,
                selectedHero: false,
            };
        case FETCHING_SUPERHEROES_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                selectedHero: false,
            };
        case SELECT_SUPERHERO:
            return{
                ...state,
                selectedHero: action.payload.id,
            };
        default:
            return state;
    }
};

export default superheroesReducer;
