import { IError } from '../../common/interfaces/IError';

export const selectData = (state: any): any => state.getIn(['superheroesReducer', 'data']);
export const selectError = (state: any): IError => state.getIn(['superheroesReducer', 'error']);
export const selectLoading = (state: any): boolean => state.getIn(['superheroesReducer', 'loading']);
export const selectHighlighted = (state: any): number => state.getIn(['superheroesReducer', 'selectedHero']);
