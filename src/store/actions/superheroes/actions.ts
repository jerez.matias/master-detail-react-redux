import IAction, { IEmptyAction, IFailureAction } from '../../../common/interfaces/IAction';
import IError from '../../../common/interfaces/IError';
import { FETCHING_SUPERHEROES, FETCHING_SUPERHEROES_ERROR, FETCHING_SUPERHEROES_SUCCESS,
    SELECT_SUPERHERO } from './constants';

export interface IGetSuperheroesSuccessAction extends IAction<{ data: any }> {}
export interface IGetSuperheroesQueryAction extends IAction<{ query: string }> {}

export const getSuperheroes = (query: string): IGetSuperheroesQueryAction => ({
    type: FETCHING_SUPERHEROES,
    payload: {
        query,
    },
});

export const getSuperheroesSuccess = (data: any): IGetSuperheroesSuccessAction => ({
    type: FETCHING_SUPERHEROES_SUCCESS,
    payload: {
        data,
    },
});

export const getSuperheroesError = (error: IError): IFailureAction => ({
    type: FETCHING_SUPERHEROES_ERROR,
    payload: {
        error,
    },
});

export const selectSuperhero = (id: number) => ({
    type: SELECT_SUPERHERO,
    payload: {
        id,
    },
});
