export const FETCHING_SUPERHEROES = 'FETCHING_SUPERHEROES';
export const FETCHING_SUPERHEROES_SUCCESS = 'FETCHING_SUPERHEROES_SUCCESS';
export const FETCHING_SUPERHEROES_ERROR = 'FETCHING_SUPERHEROES_ERROR';
export const SELECT_SUPERHERO = 'SELECT_SUPERHERO';
