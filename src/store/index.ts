import { fromJS } from 'immutable';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import superheroesSagas from './sagas/superheroes';

let store: any;
const sagas: any = [
    superheroesSagas,
];

export const initStore = (reducers: any, history: any) => {
    store = configureStore(reducers, history);
    return store;
};

export const configureStore = (reducers: any, history: any, initialState = {}) => {
    const sagaMiddleware = createSagaMiddleware();
    const devtools = (window as any).devToolsExtension || (() => (noop: any) => noop);

    const middlewares = [routerMiddleware(history), sagaMiddleware];
    // const enhancers = [applyMiddleware(...middlewares), devtools()];

    store = createStore(
        reducers,
        fromJS(initialState), // eslint-disable-next-line
        compose(
            applyMiddleware(...middlewares),
            devtools(),
        ),
    );

    for (const saga of sagas) {
        sagaMiddleware.run(saga);
    }

    return store;
};

export const getStore = () => {
    return store;
};
