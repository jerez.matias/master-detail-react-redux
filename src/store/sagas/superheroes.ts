import { put, takeLatest } from 'redux-saga/effects';

import { getSuperheroes, searchSuperheroes } from '../../services/superhero.service';
import { FETCHING_SUPERHEROES, getSuperheroesError, getSuperheroesSuccess } from '../actions/superheroes';

export function* getSuperheroesData(action: any) {
    try {
        const data = yield searchSuperheroes(action.payload.query);
        yield put(getSuperheroesSuccess(data.results));
    } catch (e) {
        yield put(getSuperheroesError(e));
    }
}

export default function* watch(q: any) {
    yield takeLatest(FETCHING_SUPERHEROES, (actionModifier: any) => getSuperheroesData(actionModifier));
}
