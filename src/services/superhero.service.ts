import axios from 'axios';

const apiBaseUrl = 'https://www.superheroapi.com/api.php/1444475025695318/search/batman';
const apiBaseSearch = 'https://www.superheroapi.com/api.php/1444475025695318/search/';
export const getSuperheroes = (): Promise<any> =>
    axios.get(apiBaseUrl)
        .then((response: any) => response.data);

export const searchSuperheroes = (query: string): Promise<any> =>
    axios.get(apiBaseSearch + query)
        .then((response: any) => response.data);
